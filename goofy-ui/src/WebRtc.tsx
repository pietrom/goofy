import React, {useState} from 'react'

const constraints = {
    audio: true,
    video: true
}

const offerOptions: RTCOfferOptions = {
    offerToReceiveAudio: true,
    offerToReceiveVideo: true
}

function WebRtc() {
    const [videoRef] = useState(React.createRef<HTMLVideoElement>())
    const [recordedVideoRef] = useState(React.createRef<HTMLVideoElement>())
    const [recordedBlobs, setRecordedBlobs] = useState([] as Blob[])

    const stream = (): MediaStream | null => {
        return videoRef && videoRef.current && videoRef.current.srcObject as MediaStream
    }

    const setStream = (stream: MediaStream | null) => {
        if (videoRef.current) {
            videoRef.current.srcObject = stream;
        }
    }

    const clearStream = () => setStream(null)

    const handleSuccess = (stream: MediaStream) => {
        const video = document.querySelector('video');
        const videoTracks = stream.getVideoTracks();
        console.log('Got stream with constraints:', constraints);
        console.log(`Using video device: ${videoTracks[0].label}`);
        setStream(stream)
    }
    const handleError = (err: any) => {
        console.error(err)
    }
    const openCamera = async (e: React.MouseEvent<HTMLElement>) => {
        try {
            const stream = await navigator.mediaDevices.getUserMedia(constraints);
            handleSuccess(stream)
            e.stopPropagation()
            e.preventDefault()
        } catch (e) {
            handleError(e);
        }
    }

    const startVideoSharing = async (e: React.MouseEvent<HTMLElement>) => {
        try {
            const mediaDevices = navigator.mediaDevices as any
            const stream = await mediaDevices.getDisplayMedia({video: true})
            handleSuccess(stream)
            e.stopPropagation()
            e.preventDefault()
        } catch (e) {
            handleError(e);
        }
    }

    const closeCamera = () => {
        const theStream = stream()
        if (theStream) {
            theStream.getTracks().forEach(t => t.stop())
            clearStream()
            clearPlayer()
        }
        setRecordedBlobs(() => [])
    }

    let mediaRecorder: MediaRecorder | undefined = undefined

    function handleDataAvailable(event: BlobEvent) {
        console.log('handleDataAvailable', event);
        if (event.data && event.data.size > 0) {
            setRecordedBlobs(blobs => [...blobs, event.data])
        }
    }

    const recordedVideo = (): HTMLVideoElement | null => {
        return recordedVideoRef && recordedVideoRef.current
    }

    function play() {
        const superBuffer = new Blob(recordedBlobs, {type: 'video/webm'});
        const theVideo = recordedVideo()
        if(theVideo) {
            theVideo.src = "";
            theVideo.srcObject = null;

            theVideo.src = URL.createObjectURL(superBuffer);
            theVideo.controls = true;
            theVideo.play();
        }
    }

    function clearPlayer() {
        const theVideo = recordedVideo()
        if(theVideo && theVideo.src) {
            theVideo.src = "";
            theVideo.srcObject = null;
        }
    }

    function startRecording() {
        // let recordedBlobs = [];
        let options = {mimeType: 'video/webm;codecs=vp9,opus'};
        if (!MediaRecorder.isTypeSupported(options.mimeType)) {
            console.error(`${options.mimeType} is not supported`);
            options = {mimeType: 'video/webm;codecs=vp8,opus'};
            if (!MediaRecorder.isTypeSupported(options.mimeType)) {
                console.error(`${options.mimeType} is not supported`);
                options = {mimeType: 'video/webm'};
                if (!MediaRecorder.isTypeSupported(options.mimeType)) {
                    console.error(`${options.mimeType} is not supported`);
                    options = {mimeType: ''};
                }
            }
        }

        try {
            if (videoRef.current && videoRef.current.srcObject) {
                mediaRecorder = new MediaRecorder(videoRef.current.srcObject as MediaStream, options);
                mediaRecorder.onstop = (event) => {
                    console.log('Recorder stopped: ', event);
                    console.log('Recorded Blobs: ', recordedBlobs);
                };
                mediaRecorder.ondataavailable = handleDataAvailable;
                mediaRecorder.start();
                console.log('MediaRecorder started', mediaRecorder);
            }
        } catch (e) {
            console.error('Exception while creating MediaRecorder:', e);
            return;
        }


    }

    function stopRecording() {
        if (mediaRecorder) {
            mediaRecorder.stop();
        }
    }

    let pc1: RTCPeerConnection | null = null
    let pc2: RTCPeerConnection | null = null

    async function runPeerConnection() {
        console.log('Starting call');
        const localStream = stream()
        if(localStream) {
            const videoTracks = localStream.getVideoTracks();
            const audioTracks = localStream.getAudioTracks();
            if (videoTracks.length > 0) {
                console.log(`Using video device: ${videoTracks[0].label}`);
            }
            if (audioTracks.length > 0) {
                console.log(`Using audio device: ${audioTracks[0].label}`);
            }
            const configuration = {};
            console.log('RTCPeerConnection configuration:', configuration);
            pc1 = new RTCPeerConnection(configuration);
            console.log('Created local peer connection object pc1');
            pc1.addEventListener('icecandidate', e => onIceCandidate(pc1!, e));
            pc2 = new RTCPeerConnection(configuration);
            console.log('Created remote peer connection object pc2');
            pc2.addEventListener('icecandidate', e => onIceCandidate(pc2!, e));
            pc1.addEventListener('iceconnectionstatechange', e => onIceStateChange(pc1!, e));
            pc2.addEventListener('iceconnectionstatechange', e => onIceStateChange(pc2!, e));
            pc2.addEventListener('track', gotRemoteStream);

            localStream.getTracks().forEach(track => pc1 && pc1.addTrack(track, localStream));
            console.log('Added local stream to pc1');

            try {
                console.log('pc1 createOffer start');
                const offer = await pc1.createOffer(offerOptions);
                await onCreateOfferSuccess(offer);
            } catch (e) {
                onCreateSessionDescriptionError(e);
            }
        }
    }

    function onCreateSessionDescriptionError(error: any) {
        console.log(`Failed to create session description: ${error.toString()}`);
    }

    async function onCreateOfferSuccess(desc: RTCSessionDescriptionInit) {
        console.log(`Offer from pc1\n${desc.sdp}`);
        console.log('pc1 setLocalDescription start');
        try {
            await pc1?.setLocalDescription(desc)
            onSetLocalSuccess(pc1!)
        } catch (e) {
            onSetSessionDescriptionError(e)
        }

        console.log('pc2 setRemoteDescription start');
        try {
            await pc2?.setRemoteDescription(desc)
            onSetRemoteSuccess(pc2!);
        } catch (e) {
            onSetSessionDescriptionError(e);
        }

        console.log('pc2 createAnswer start');
        // Since the 'remote' side has no media stream we need
        // to pass in the right constraints in order for it to
        // accept the incoming offer of audio and video.
        try {
            const answer = await pc2?.createAnswer()
            await onCreateAnswerSuccess(answer!);
        } catch (e) {
            onCreateSessionDescriptionError(e);
        }
    }

    function onSetLocalSuccess(pc: RTCPeerConnection) {
        console.log(`${getName(pc)} setLocalDescription complete`);
    }

    function onSetRemoteSuccess(pc: RTCPeerConnection) {
        console.log(`${getName(pc)} setRemoteDescription complete`);
    }

    function onSetSessionDescriptionError(error: any) {
        console.log(`Failed to set session description: ${error.toString()}`);
    }

    function getName(pc: RTCPeerConnection) {
        return (pc === pc1) ? 'pc1' : 'pc2';
    }

    function getOtherPc(pc: RTCPeerConnection): RTCPeerConnection {
        return ((pc === pc1) ? pc2 : pc1)!;
    }

    function gotRemoteStream(e: any) {
        const remoteVideo = recordedVideo()
        if (remoteVideo && remoteVideo.srcObject !== e.streams[0]) {
            remoteVideo.srcObject = e.streams[0];
            console.log('pc2 received remote stream');
        }
    }

    async function onCreateAnswerSuccess(desc: RTCSessionDescriptionInit) {
        console.log(`Answer from pc2:\n${desc.sdp}`);
        console.log('pc2 setLocalDescription start');
        try {
            await pc2?.setLocalDescription(desc)
            onSetLocalSuccess(pc2!);
        } catch (e) {
            onSetSessionDescriptionError(e);
        }
        console.log('pc1 setRemoteDescription start');
        try {
            await pc1?.setRemoteDescription(desc);
            onSetRemoteSuccess(pc1!)
        } catch (e) {
            onSetSessionDescriptionError(e);
        }
    }

    async function onIceCandidate(pc: RTCPeerConnection, event: RTCPeerConnectionEventMap["icecandidate"]) {
        try {
            await (getOtherPc(pc).addIceCandidate((event.candidate)!))
            onAddIceCandidateSuccess(pc);
        } catch (e) {
            onAddIceCandidateError(pc, e);
        }
        console.log(`${getName(pc)} ICE candidate:\n${event.candidate ? event.candidate.candidate : '(null)'}`);
    }

    function onAddIceCandidateSuccess(pc: RTCPeerConnection) {
        console.log(`${getName(pc)} addIceCandidate success`);
    }

    function onAddIceCandidateError(pc: RTCPeerConnection, error: any) {
        console.log(`${getName(pc)} failed to add ICE Candidate: ${error.toString()}`);
    }

    function onIceStateChange(pc: RTCPeerConnection, event: any) {
        if (pc) {
            console.log(`${getName(pc)} ICE state: ${pc.iceConnectionState}`);
            console.log('ICE state change event: ', event);
        }
    }

    function hangup() {
        console.log('Ending call');
        pc1?.close();
        pc2?.close();
        pc1 = null;
        pc2 = null;
    }

    return <div style={{border: '1px solid white'}}>
        <video style={{ width: '45%', margin: '5px'}} ref={videoRef} autoPlay playsInline></video>
        <video style={{ width: '45%'}} ref={recordedVideoRef} autoPlay playsInline></video>
        <hr />
        <button onClick={openCamera}>Open camera</button>
        <button onClick={startVideoSharing}>Start video sharing</button>
        <button onClick={closeCamera}>Close camera</button>
        <button onClick={startRecording}>Start recording</button>
        <button onClick={stopRecording}>Stop recording</button>
        <button onClick={play}>Play</button>
        <button onClick={runPeerConnection}>Peer Connection</button>
        <button onClick={hangup}>Hangup</button>
    </div>
}

export default WebRtc
