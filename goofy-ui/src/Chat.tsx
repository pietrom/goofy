import React, { useState, useEffect } from 'react';
import './App.css';
import {GoofyConfiguration} from "./config";

interface ChatProps {
    config: GoofyConfiguration
    user: string
}

function Chat({ user, config }: ChatProps) {
    const [ messages, setMessages ] = useState([] as string[])
    const [text, setText] = useState('')
    const [socket, setSocket] = useState<WebSocket | null>(null)

    useEffect(() => {
        console.log('Opening WS')
        const socket = new WebSocket(`${config.wsEndpoint}/chat`);
        socket.addEventListener('open', function (event) {
            socket.send(`${user}|Hello`);
        });
        socket.addEventListener('message', function (msgEvent) {
            const data = JSON.parse(msgEvent.data)
            setMessages(msgs => msgs.concat([`${data.from}:: ${data.text}`]).slice(-8))
        });
        setSocket(socket)
    }, [user]);

    const textChanged = (evt: React.ChangeEvent<HTMLInputElement>) => {
        setText(evt.currentTarget.value)
    }

    const send = (event: React.MouseEvent<HTMLButtonElement>) => {
        if (socket != null) {
            socket.send(`${user}|${text}`)
            setText('')
        }
    }

    return (<>
        <span>Messaggi:</span>
        <ul>
            {messages.map((msg, i) => <li key={i}>{msg}</li>)}
        </ul>
        <input type="text" onChange={textChanged} value={text} />
        <button onClick={send}>Invia</button>  
        </>
    );
}

export default Chat;
