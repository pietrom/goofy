import React, { useContext } from 'react'
import { AuthenticationContext } from './AuthenticationContext'

function UserProfile() {
  const context = useContext(AuthenticationContext)
  return (<>
    <p>
        Logged in? {context.isLogged ? 'true' : 'false'}
      </p>
      <p>
        Username: {context.username}
      </p>
      <p>
          User info: {context.firstName} {context.lastName}
      </p>
      <p>
        <button onClick={context.login}>Login</button>            
        <button onClick={context.logout}>Logout</button>            
      </p>
  </>)
}          

export default UserProfile
