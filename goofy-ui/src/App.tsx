import React, { useState, createContext } from 'react'
import logo from './logo.svg'
import './App.css'
import Routing from './Routing'
import { useTranslation } from 'react-i18next'
import AuthenticationContextSchema, { AuthenticationContext } from './AuthenticationContext'
import {GoofyConfiguration} from "./config";

function App({ config }: { config: GoofyConfiguration }) {  
  const [ ctx, setCtx ] = useState<AuthenticationContextSchema>({
    isLogged: false,
    username: '',
    firstName: '',
    lastName: '',
    logout: () => {
      setCtx(current => Object.assign({}, current, {
        isLogged: false,
        username: '',
        firstName: '',
        lastName: '',
      }))
    },
    login: () => {
      setCtx(current => Object.assign({}, current, {
        isLogged: true,
        username: 'pietrom',
        firstName: 'Pietro',
        lastName: 'Martinelli',
      }))
    }
  })

  return (
    <div className="App">
      <header className="App-header">
        {/*<img src={logo} className="App-logo" alt="logo" />*/}
        <AuthenticationContext.Provider value={ctx}>
          <Routing config={config} />
        </AuthenticationContext.Provider>
      </header>
    </div>
  );
}

export default App;
