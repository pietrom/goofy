import React from 'react';

interface ClockProps {
    now: Date;
}

function render(date: Date) {
    return date.toISOString()
}

function Clock(props : ClockProps) {
  return (
    <span>It's {render(props.now || new Date())}</span>
  );
}

export default Clock;
