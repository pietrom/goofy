import React from 'react'
import Events from './Events';
import {GoofyConfiguration} from "./config";

const randomId = () => Math.floor(Math.random() * 10000)

const ServerSentEventsContainer = (props: { config: GoofyConfiguration }) => <>
    <Events config={props.config} subscriber={`abcd_${randomId()}`} />
    <Events config={props.config}subscriber={`xxyyzz_${randomId()}`} />
</>

export default ServerSentEventsContainer