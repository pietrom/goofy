const endpointOf = (protocol: string, hostAndPort: string) => `${protocol}://${hostAndPort}`

// const host = 'db660e4ef2c3.ngrok.io'
// const apiProtocol = 'https'
// const wsProtocol = 'wss'

const host = 'localhost:19000'
const apiProtocol = 'http'
const wsProtocol = 'ws'

export const Config = {
    apiEndpoint: endpointOf(apiProtocol, host),
    wsEndpoint: endpointOf(wsProtocol, host)
} as GoofyConfiguration

export interface GoofyConfiguration {
    apiEndpoint: string
    wsEndpoint: string
}
