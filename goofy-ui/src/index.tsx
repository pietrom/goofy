import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import initI18n from './i18n'
import { Config } from './config'
const i18n = initI18n(Config)
console.log(i18n)

// moment.locale(i18n.language)

ReactDOM.render(
  <React.StrictMode>
      <React.Suspense fallback="Loading...">
        <App config={Config} />
      </React.Suspense>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
