import React from 'react'

interface HelloProps {
    to: string
}
function Hello({ to } : HelloProps) {
  return (
      <span>Hello, {to}!</span>
  )
}

export default Hello
