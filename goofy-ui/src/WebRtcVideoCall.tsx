import React, {ChangeEvent, useEffect, useState} from 'react'
import {IceCandidateMessage, Message, OfferMessage, VideoAnswerMessage} from "./WebRtcMessages";

const shareAudio = false

const log = console.log

const mediaConstraints = {
    audio: shareAudio,
    video: true
}

const offerOptions: RTCOfferOptions = {
    offerToReceiveAudio: shareAudio,
    offerToReceiveVideo: true
}

// const connectionConfig: RTCConfiguration = {
//     iceServers: [{
//         urls: ['stun:stun.l.google.com:19302']
//     }]
// }

//
const connectionConfig: RTCConfiguration = {
    iceServers: [{
             urls: ['stun:stun.l.google.com:19302']
         }]
}

function handlerFor(descr: string): (this: RTCPeerConnection, e: Event) => any {
    return function (e: Event) {
        console.log(`${descr}: ${e}`)
    }
}

interface WebRtcVideoCallProps {
    socket: WebSocket
}

function WebRtcVideoCall({ socket }: WebRtcVideoCallProps) {
    const [localVideoRef] = useState(React.createRef<HTMLVideoElement>())
    const [remoteVideoRef] = useState(React.createRef<HTMLVideoElement>())
    const [username, setUsername] = useState('webuser')

    const setPeerConnection = (pc: RTCPeerConnection) => {
        const win = window as any
        win.peerConnection = pc
        console.log('window.peerConnection', win.peerConnection)
    }

    const peerConnection = (): RTCPeerConnection => {
        const win = window as any
        console.log('Getting peerConnection from window', win.peerConnection)
        return (window as any).peerConnection as RTCPeerConnection
    }

    useEffect(() => {
        socket.addEventListener('message', async function (msgEvent) {
            const data = JSON.parse(msgEvent.data) as Message
            console.log(` message ${data.type}`)
            await handleIncomingMsg(data)
        })
    }, [socket])



    const sendToServer = (msg: Message) => {
        console.log(`SEND MESSAGE ${msg.type}`)
        socket.send(JSON.stringify(msg))
    }

    const onUsernameChanged = (e: ChangeEvent<HTMLInputElement>): void => {
        setUsername(e.target.value)
    }

    async function handleNegotiationNeededEvent(this: RTCPeerConnection, e: Event) {
        const offer = await this.createOffer()
        await this.setLocalDescription(offer)
        const msg = {
            name: username,
            type: "video-offer",
            sdp: this.localDescription
        } as OfferMessage
        sendToServer(msg)
    }

    async function handleIncomingMsg(rawMessage: Message) {
        if(rawMessage.type === "video-offer") {
            const msg: OfferMessage = rawMessage as OfferMessage
            console.log('handle video-offer', msg)
            const theStream = localStream() as MediaStream
            console.log(msg, theStream)
            if(msg && theStream) {
                const thePc = createPeerConnection()
                setPeerConnection(thePc)
                console.log('PC created handling video-offer')
                const desc = new RTCSessionDescription(msg.sdp);
                await thePc.setRemoteDescription(desc)
                const stream = await navigator.mediaDevices.getUserMedia(mediaConstraints)
                setLocalStream(stream)
                theStream.getTracks().forEach(track => thePc!.addTrack(track, theStream))
                const answer = await thePc.createAnswer()
                await thePc.setLocalDescription(answer)
                console.log(7)
                const outMsg = {
                    name: username,
                    type: "video-answer",
                    sdp: thePc.localDescription
                } as VideoAnswerMessage
                console.log(8)
                sendToServer(outMsg)
                console.log(9)
            }
        } else if(rawMessage.type === "new-ice-candidate") {
            const msg: IceCandidateMessage = rawMessage as IceCandidateMessage
            console.log('handle new-ice-candidate', msg, peerConnection())
            try {
                if(peerConnection()) {
                    await peerConnection().addIceCandidate(msg.candidate)
                } else {
                    console.error('Received handle new-ice-candidate but peerConnection is null')
                }
                console.log('1111 --> ', msg.candidate, 'pc = ', peerConnection())
            } catch(err) {
                console.log('0000 --> ', msg.candidate, '|', err, '|', JSON.stringify(err), '|', 'pc = ', peerConnection())
            }
        } else if (rawMessage.type === "video-answer") {
            const msg: VideoAnswerMessage = rawMessage as VideoAnswerMessage
            var desc = new RTCSessionDescription(msg.sdp)
            try {
                await peerConnection().setRemoteDescription(desc)
            } catch(err) {
                console.error('Error handling video-answer', err)
            }

        }
    }

    function handleTrackEvent(event: RTCTrackEvent) {
        console.log('!!! TRACK EVENT !!!', event.streams.length, event.streams[0])
        event.streams[0].onaddtrack = (ev: MediaStreamTrackEvent) => {
          console.log('track added')
        }
        setRemoteStream(event.streams[0])
    }

    function handleICECandidateEvent(event: RTCPeerConnectionIceEvent) {
        if (event.candidate) {
            sendToServer({
                type: "new-ice-candidate",
                name: username,
                candidate: event.candidate
            } as IceCandidateMessage);
        }
    }

    function createPeerConnection(): RTCPeerConnection {
        const thePc = new RTCPeerConnection(connectionConfig)
        thePc.onicecandidate = handleICECandidateEvent
        thePc.ontrack = handleTrackEvent
        thePc.onnegotiationneeded = handleNegotiationNeededEvent
        // thePc.onremovetrack = handlerFor('onremovetrack')
        thePc.oniceconnectionstatechange = handlerFor('oniceconnectionstatechange')
        thePc.onicegatheringstatechange = handlerFor('onicegatheringstatechange')
        thePc.onsignalingstatechange = handlerFor('onsignalingstatechange')
        return thePc
    }


    const localStream = (): MediaStream | null => {
        return localVideoRef && localVideoRef.current && localVideoRef.current.srcObject as MediaStream
    }

    const setLocalStream = (stream: MediaStream | null) => {
        if (localVideoRef.current) {
            localVideoRef.current.srcObject = stream;
        }
    }

    const setRemoteStream = (stream: MediaStream | null) => {
        if (remoteVideoRef.current) {
            remoteVideoRef.current.srcObject = stream;
        }
    }

    const clearStream = () => setLocalStream(null)

    const handleSuccess = (stream: MediaStream) => {
        const video = document.querySelector('video');
        const videoTracks = stream.getVideoTracks();
        console.log('Got stream with constraints:', mediaConstraints);
        console.log(`Using video device: ${videoTracks[0].label}`);
        setLocalStream(stream)
    }
    const handleError = (err: any) => {
        console.error(err)
    }
    const openCamera = async (e: React.MouseEvent<HTMLElement>) => {
        try {
            const stream = await navigator.mediaDevices.getUserMedia(mediaConstraints);
            handleSuccess(stream)
            e.stopPropagation()
            e.preventDefault()
        } catch (e) {
            handleError(e);
        }
    }

    const closeCamera = () => {
        const theStream = localStream()
        if (theStream) {
            theStream.getTracks().forEach(t => t.stop())
            clearStream()
        }
    }

    function startCall() {
        console.log('Starting call')
        const thePc = createPeerConnection()
        console.log('PC created')
        const stream = localStream()
        if(stream) {
            stream.getTracks().forEach(track => thePc.addTrack(track, stream))
        }
        setPeerConnection(thePc)
    }

    // function hangup() {
    //     console.log('Ending call');
    //     closeCamera()
    // }

    return <div style={{border: '1px solid white', width: '80%'}}>
            <video style={{ width: '45%', margin: '5px'}} ref={localVideoRef} autoPlay playsInline></video>
            <video style={{ width: '45%'}} ref={remoteVideoRef} autoPlay playsInline></video>
        <hr />
        <div style={{width: '100%', height: '50px'}}>
            <button onClick={openCamera}>Open camera</button>
            <button onClick={startCall}>Start call</button>
        </div>
    </div>
}

export default WebRtcVideoCall
