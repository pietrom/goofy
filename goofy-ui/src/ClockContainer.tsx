import React, { useState, useEffect } from 'react'
import Clock from './Clock'
import axios from 'axios'
import { GoofyConfiguration } from './config'

const ClockContainer = ({config}:{config:GoofyConfiguration}) => {
    const [now, setNow] = useState(new Date())

    async function fetchClockData() {
        const { data } = await axios.get(`${config.apiEndpoint}/clock`)
        setNow(new Date(data.now))
    }

    useEffect(() => {
        fetchClockData()
    }, [])

    return <Clock now={now} />
}

export default ClockContainer
