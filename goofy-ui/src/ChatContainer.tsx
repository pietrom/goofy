import React from 'react'
import Chat from './Chat';
import './ChatContainer.css'
import {GoofyConfiguration} from "./config";

const ChatContainer = ({ config }: { config: GoofyConfiguration }) => <>
    <div className="row">
        <div className="column"><Chat config={config} user="pietrom"/></div>
        <div className="column"><Chat config={config} user="martinellip"/></div>
    </div>
</>

export default ChatContainer