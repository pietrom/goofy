const Backend = {
    type: 'backend',
    init: function (services, backendOptions, i18nextOptions) {
        /* use services and options */
        this.data = backendOptions.data || {}
    },
    read: function (language, namespace, callback) {
        console.log('in mem read', language, namespace)
        if(this.data[language] && this.data[language][namespace]) {
            console.log('providing', this.data[language][namespace])
            callback(null, this.data[language][namespace])
        } else {
            callback(null, null)
        }
        /* return resources */
        // callback(null, {
        //     key: 'value'
        // });

        /* if method fails/returns an error, call this: */
        /* callback(truthyValue, null); */
    },

    // optional
    readMulti: function (languages, namespaces, callback) {
        console.log(namespaces)
        /* return multiple resources - useful eg. for bundling loading in one xhr request */
        callback(null, {
            en: {
                translations: {
                    key: 'value'
                }
            },
            de: {
                translations: {
                    key: 'value'
                }
            }
        });

        /* if method fails/returns an error, call this: */
        /* callback(truthyValue, null); */
    },

    // only used in backends acting as cache layer
    save: function (language, namespace, data) {
        // store the translations
    },

    create: function (languages, namespace, key, fallbackValue) {
        /* save the missing translation */
    }
}

export default Backend