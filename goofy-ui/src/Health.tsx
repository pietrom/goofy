import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { GoofyConfiguration } from './config'

function Health({config}:{config:GoofyConfiguration}) {
  const [health, setHealth] = useState({})

  async function fetchHealthData() {
    const { data } = await axios.get(`${config.apiEndpoint}/health`)
    setHealth(data)
  }

  const data = JSON.stringify(health)

  useEffect(() => {
      fetchHealthData()
  }, [])

  return (
      <span>{data}</span>
  )

}

export default Health
