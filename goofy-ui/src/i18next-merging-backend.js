import * as utils from './i18next-utils';
import deepmerge from 'deepmerge'

function after(times, callback) {
    return function () {
        times--;
        if (times === 0) {
            callback();
        }
    }
}

function asyncmap(list, op, cb) {
    let results = []
    let errors = [];
    const done = after(list.length, function () {
        console.log('DONE Calling callback', errors, results)
        cb(errors.length ? errors : null, results.filter(r => r));
    });

    list.forEach(function (item, i) {
        op(item, function (err, result) {
            if (err) {
                errors.push(err)
                console.log('Errors', results)
            } else {
                results.push(result)
                console.log('Results', results)
            }
            done()
        });
    });
}

function getDefaults() {
    return {};
}

class Backend {
    constructor(services, options = {}) {
        this.backends = [];
        this.type = 'backend';

        this.init(services, options);
    }

    init(services, options = {}, i18nextOptions) {
        this.services = services;
        this.options = utils.defaults(options, this.options || {}, getDefaults());

        this.options.backends && this.options.backends.forEach((b, i) => {
            this.backends[i] = this.backends[i] || utils.createClassOnDemand(b);
            this.backends[i].init(services, this.options.backendOptions[i], i18nextOptions);
        })
    }

    read(language, namespace, callback) {
        asyncmap(this.backends, (be, cb) => {
            console.log('Calling wrapped backend', be)
            be.read(language, namespace, cb)
        }, (err, results) => {
            console.log('COMPLETED')
            if (err) {
                console.log('ERROR', err)
                if(!results.length) {
                    callback(err)
                }
            }
            if(results.length) {
                console.log('RESULTS', results)
                const merged = results.reduce((acc, curr) => deepmerge(acc, curr), {})
                console.log('read. Lang =', language, 'NS =', namespace, 'RESULTS =', results, 'MERGED =', merged)
                callback(null, merged)
            }
        })
    }


    create(languages, namespace, key, fallbackValue) {
        this.backends.forEach(b => {
            if (b.create) b.create(languages, namespace, key, fallbackValue);
        });
    }
}

Backend.type = 'backend';


export default Backend;