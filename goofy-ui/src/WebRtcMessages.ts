export interface OfferMessage {
    name: string,
    type: 'video-offer'
    sdp: RTCSessionDescription
}

export interface VideoAnswerMessage {
    name: string,
    type: 'video-answer'
    sdp: RTCSessionDescription
}

export interface IceCandidateMessage {
    name: string,
    type: 'new-ice-candidate'
    candidate: RTCIceCandidate
}

export type Message = OfferMessage | IceCandidateMessage | VideoAnswerMessage