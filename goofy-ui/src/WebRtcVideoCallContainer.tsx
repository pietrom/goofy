import React, {useEffect, useState} from 'react'
import WebRtcVideoCall from "./WebRtcVideoCall"
import {GoofyConfiguration} from "./config";

interface WebRtcVideoCallContainerProps {
  config: GoofyConfiguration
}

function WebRtcVideoCallContainer({ config }: WebRtcVideoCallContainerProps) {
  const [socket, setSocket] = useState<WebSocket | null>(null)
  useEffect(() => {
    console.log('Opening WS (effect)')
    const theSocket = new WebSocket(`${config.wsEndpoint}/signal`)
    theSocket.addEventListener('open', function (event) {
      console.log('WSS opened', socket)
    })
    setSocket(theSocket)
  }, [])
  return socket ? <WebRtcVideoCall socket={socket} /> : <></>
}

export default WebRtcVideoCallContainer
