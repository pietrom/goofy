export default {
    translations: {
        home: {
            title: 'Pagina principale',
            en: 'Inglese',
            it: 'Italiano',
            es: 'Spagnolo'
        }
    }
}
