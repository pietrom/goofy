export default {
    type: 'languageDetector',
    async: true, // If this is set to true, your detect function receives a callback function that you should call with your language, useful to retrieve your language stored in AsyncStorage for example
    init: function(services, detectorOptions, i18nextOptions) {
      /* use services and options */
      console.log('init my-detector', i18nextOptions)
    },
    detect: function(callback) { // You'll receive a callback if you passed async true
      /* return detected language */
      // callback('de'); if you used the async flag
    //   return 'de';
      callback('it')
    },
    cacheUserLanguage: function(lng) {
      console.log('cacheUserLanguage', lng)
    }
  }