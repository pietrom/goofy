export default {
  translations: {
    home: {
      title: 'Página de inicio',
      en: 'Inglés',
      it: 'Italiano',
      es: 'Español'
    }
  }
}
