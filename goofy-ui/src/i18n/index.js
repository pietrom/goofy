import i18n from 'i18next'
import {initReactI18next} from 'react-i18next'
import LanguageDetector from 'i18next-browser-languagedetector'
import myDetector from './my-detector'

import en from './en.js'
import it from './it.js'
import es from './es.js'

import HttpApi from 'i18next-http-backend';
import MergingBackend from '../i18next-merging-backend'
// import MergingBackend from 'i18next-chained-backend'
// import FsBacd so ckend from 'i18next-fs-backend'
import InMemoryBackend from '../i18next-inmemory-backend'

export default function initI18n(config) {
    i18n
        // .use(LanguageDetector)
        // .use(myDetector)
        .use(initReactI18next)
        .use(MergingBackend)
        .init({
            //resources: { en, it, es },
            lng: 'it-IT',
            fallbackLng: 'it-IT',
            ns: ["translations"],
            defaultNS: "translations",
            backend: {
                backends: [InMemoryBackend, HttpApi],
                backendOptions: [{
                    data: { it, en, es }
                }, {
                    loadPath: config.apiEndpoint + '/api/i18n/{{lng}}',
                    allowMultiLoading: false
                }]
            }
        });
    return i18n
}