export default {
  translations: {
    home: {
      title: 'Home page',
      en: 'English',
      it: 'Italian',
      es: 'Spanish'
    }
  }
}
