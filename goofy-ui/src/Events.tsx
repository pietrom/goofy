import React, { useState, useEffect } from 'react';
import './App.css';
import {GoofyConfiguration} from "./config";

interface EventsProps {
    subscriber: string,
    config: GoofyConfiguration
}

function Events(props: EventsProps) {
    const [ events, setEvents ] = useState([] as MessageEvent[] )

    useEffect(() => {
        const source = new EventSource(`${props.config.apiEndpoint}/events/hearth-beat/register?key=${props.subscriber}`);
        source.onmessage = (event) => {
            setEvents(eventList => eventList.concat([event]).slice(-5))
        }
        return () => {
            console.log('unmount Events', props.subscriber)
            source.close()
        }
    }, [props.subscriber]);

    return (<>
        <span>Eventi:</span>
        <ul>
            {events.map(evt => <li key={evt.lastEventId}>{evt.lastEventId}: {evt.data}</li>)}
        </ul>
        </>
    );
}

export default Events;
