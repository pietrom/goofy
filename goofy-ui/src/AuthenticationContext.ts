import React from 'react'

interface AuthenticationContextSchema {
    isLogged: boolean
    username: string
    firstName: string
    lastName: string
    logout: () => void
    login: () => void
}

export default AuthenticationContextSchema

export const DefaultAuthenticationContext = {
    isLogged: false,
    username: '',
    firstName: '',
    lastName: '',
    logout: () => {},
    login: () => {}
}

export const AuthenticationContext = React.createContext<AuthenticationContextSchema>(DefaultAuthenticationContext)
