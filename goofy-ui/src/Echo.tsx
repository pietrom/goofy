import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { GoofyConfiguration } from './config'

interface EchoResult {
    message: string;
}

function Echo({ config }: { config: GoofyConfiguration }) {
  const [msg, setMsg] = useState({} as EchoResult)

  async function fetchEchoData() {
    const { data } = await axios.get<EchoResult>(`${config.apiEndpoint}/api/echo?msg=Test message`)
    console.log('echo data', data)
    setMsg(data)
  }

  useEffect(() => {
      fetchEchoData()
  }, [])

  return (
      <span>{msg.message}</span>
  )

}

export default Echo
