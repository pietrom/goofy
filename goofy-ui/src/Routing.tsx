import React, { useState, ReactNode } from 'react'
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
import ClockContainer from './ClockContainer'
import ServerSentEventsContainer from './ServerSentEventsContainer';
import ChatContainer from './ChatContainer';
import Health from './Health';
import Echo from './Echo';
import UserProfile from './UserProfile'
import Hello from './Hello';
import WebRtc from './WebRtc'
import WebRtcVideoCallContainer from './WebRtcVideoCallContainer'
import {GoofyConfiguration} from './config'
import { useTranslation } from 'react-i18next'

const Empty = () => {
  const [to, setTo] = useState('World')
  return <div><Hello to={to} /> (
    <button onClick={() => setTo('World') }>World</button>|
    <button onClick={() => setTo('Pietro') }>Pietro</button>|
    <button onClick={() => setTo('ReactJs') }>ReactJs</button>
  )</div>
}

const Routing = ({ config }: { config: GoofyConfiguration }) => (
  <Router>
    <div>
      <Route exact path="/" render={(props) => (<WithCommonLayout><Empty /></WithCommonLayout>)} />
      <Route path="/events" render={(props) => (<WithCommonLayout><ServerSentEventsContainer {...props} config={config} /></WithCommonLayout>)} />
      <Route path="/chat" render={(props) => (<WithCommonLayout><ChatContainer {...props} config={config} /></WithCommonLayout>)} />
      <Route path="/clock" render={() => (<WithCommonLayout><ClockContainer config={config}  /></WithCommonLayout>)} />
      <Route path="/health" render={() => (<WithCommonLayout><Health config={config}  /></WithCommonLayout>)} />
      <Route path="/echo" render={() => (<WithCommonLayout><Echo config={config}  /></WithCommonLayout>)} />
      <Route path="/self" render={() => (<WithCommonLayout><UserProfile /></WithCommonLayout>)} />
      <Route path="/webrtc" render={() => (<WithCommonLayout><WebRtc /></WithCommonLayout>)} />
      <Route path="/webrtc-call" render={(props) => (<WebRtcVideoCallContainer {...props} config={config} />) } />
    </div>
  </Router>
)

interface GenericComponentProps {
  children: JSX.Element[] | JSX.Element;
}

const WithCommonLayout = (props: GenericComponentProps) => (
  <>
    <HeaderBar />
    {props.children}
    <MenuBar />
  </>
)

const HeaderBar = () => {
  const { t, i18n } = useTranslation()
  const changeLang = (lang: string) => i18n.changeLanguage(lang)
  return <h3>
    {t('home.title')} - {t('home.subtitle')} - {t('home.welcome')} -
    <button onClick={() => changeLang('en-US')}>{t('home.en')}</button>
    <button onClick={() => changeLang('it-IT')}>{t('home.it')}</button>
    <button onClick={() => changeLang('es-ES')}>{t('home.es')}</button>
  </h3>
}

const MenuBar = () => (<div className="home-button-container">
  <div className="home-button"><Link to="/"><button>Home</button></Link></div>
  <div className="home-button"><Link to="/clock"><button>Clock</button></Link></div>
  <div className="home-button"><Link to="/events"><button>Server Sent Events</button></Link></div>
  <div className="home-button"><Link to="/chat"><button>Web Socket</button></Link></div>
  <div className="home-button"><Link to="/health"><button>Health</button></Link></div>
  <div className="home-button"><Link to="/echo"><button>Echo</button></Link></div>
  <div className="home-button"><Link to="/self"><button>Profile</button></Link></div>
  <div className="home-button"><Link to="/webrtc"><button>Web RTC</button></Link></div>
  <div className="home-button"><Link to="/webrtc-call"><button>Web RTC Call</button></Link></div>
</div>)

export default Routing
