#!/bin/bash

BUILD_FILE_TEMPLATE="goofy-core/build.gradle"
FAKE_TEST_TEMPLATE="templates/FakeTest.kt"
FAKE_CLASS_TEMPLATE="templates/FakeClass.kt"
PACKAGE_PREFIX="com.darwinsw"
PROJECT_NAME="goofy"

if [ $# -ne 1 ] ; then
  echo "Usage: ${0} NEW_MODULE_NAME"
  exit 1
fi

package="${PACKAGE_PREFIX}.${PROJECT_NAME}.$(echo ${1} | sed s/${PROJECT_NAME}-// | tr '-' '.')"
dir=$(echo ${package} | tr '.' '/')

mkdir -p "${1}/src/main/kotlin/${dir}"
mkdir -p "${1}/src/test/kotlin/${dir}"
cp ${BUILD_FILE_TEMPLATE} ${1}/build.gradle
q="'"
echo "include ${q}${1}${q}" >> settings.gradle

cat ${FAKE_TEST_TEMPLATE} | sed "s/package PACKAGE/package ${package}/" > "${1}/src/test/kotlin/${dir}/FakeTest.kt"
cat ${FAKE_CLASS_TEMPLATE} | sed "s/package PACKAGE/package ${package}/" > "${1}/src/main/kotlin/${dir}/FakeClass.kt"