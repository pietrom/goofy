package com.darwinsw.goofy.chat.web

import com.darwinsw.goofy.clock.core.ClockData

data class ChatOutputMessage(val chat: String, val sender: String, val at: ClockData, val text: String)