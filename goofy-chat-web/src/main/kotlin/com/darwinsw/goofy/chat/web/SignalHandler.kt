package com.darwinsw.goofy.chat.web

import org.springframework.stereotype.Component
import org.springframework.web.socket.CloseStatus
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.handler.TextWebSocketHandler

@Component
class SignalHandler : TextWebSocketHandler() {
    companion object {
        val SyncLock = String()

        private val Clients = mutableListOf<WebSocketSession>()
    }

    override fun afterConnectionEstablished(session: WebSocketSession) {
        super.afterConnectionEstablished(session)
        synchronized(SyncLock) {
            println("Adding session ${session.id}")
            Clients.add(session)
        }
    }

    override fun afterConnectionClosed(session: WebSocketSession, status: CloseStatus) {
        super.afterConnectionClosed(session, status)
        synchronized(SyncLock) {
            println("Removing session ${session.id}")
            Clients.removeIf { s -> s.id == session.id }
        }
    }

    public override fun handleTextMessage(session: WebSocketSession, message: TextMessage) {
        println("Receiving ${message.payload} from ${session.id}")
        synchronized(SyncLock) {
            Clients.filter { s -> s.isOpen && s.id != session.id }.forEach { s -> s.sendMessage(message) }
        }
    }
}