package com.darwinsw.goofy.chat.web

import com.darwinsw.goofy.clock.core.Clock
import org.springframework.stereotype.Component
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.handler.TextWebSocketHandler

@Component
class ChatHandler(private val clock: Clock) : TextWebSocketHandler() {
    companion object {
        val SyncLock = String()

        private val Clients = mutableListOf<WebSocketSession>()
    }


    override fun afterConnectionEstablished(session: WebSocketSession) {
        super.afterConnectionEstablished(session)
        synchronized(SyncLock) {
            Clients.add(session)
        }
    }
    public override fun handleTextMessage(session: WebSocketSession, message: TextMessage) {
        val payloadFields = message.payload.split("|")

        val textMessage = TextMessage("""{
            "from": "${payloadFields[0]}",
            "text": "${payloadFields[1]}"
        }""".trimIndent())

        synchronized(SyncLock) {
            Clients.filter { session -> session.isOpen }.forEach { session -> session.sendMessage(textMessage) }
        }
    }
}