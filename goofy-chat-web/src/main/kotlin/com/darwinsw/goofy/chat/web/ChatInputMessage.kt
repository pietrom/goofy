package com.darwinsw.goofy.chat.web

data class ChatInputMessage(val sender: String, val text: String)