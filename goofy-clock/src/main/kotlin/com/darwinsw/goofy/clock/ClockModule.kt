package com.darwinsw.goofy.clock

import com.darwinsw.goofy.clock.core.Clock
import com.darwinsw.goofy.clock.system.SystemClock
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class ClockModule {
    @Bean
    open fun clock() : Clock = SystemClock()
}