package com.darwinsw.goofy.clock.core

import java.time.ZonedDateTime

data class ClockData(val now: ZonedDateTime)