package com.darwinsw.goofy.clock.core

interface Clock {
    fun now(): ClockData
}