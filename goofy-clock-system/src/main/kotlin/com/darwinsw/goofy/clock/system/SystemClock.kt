package com.darwinsw.goofy.clock.system

import com.darwinsw.goofy.clock.core.Clock
import com.darwinsw.goofy.clock.core.ClockData
import java.time.ZonedDateTime

class SystemClock : Clock {
    override fun now(): ClockData = ClockData(ZonedDateTime.now())
}