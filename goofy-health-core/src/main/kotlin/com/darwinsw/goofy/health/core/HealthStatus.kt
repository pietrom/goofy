package com.darwinsw.goofy.health.core

data class HealthStatus(val ok: Boolean)