package com.darwinsw.goofy.health

import com.darwinsw.goofy.health.core.HealthService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class HealthModule {
    @Bean
    open fun healthService() = HealthService()
}