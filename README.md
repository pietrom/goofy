# kotlin-template
A template for multi-module Kotlin projects, using Gradle as build tool

## How to use it
### Clone the repo
```
git clone git@gitlab.com:darwinsw/kotlin-template.git
```
or
```
https://gitlab.com/darwinsw/kotlin-template.git
```
if you prefer the HTTPS URL
### Rename, enter and customize the codebase
```
mv kotlin-template your-project-name
cd your-project-name
mv kotlin-template-core your-project-name-core
mv kotlin-template-main your-project-name-main
# Change project dependency to :kotlin-template-core in kotlin-template-mane/build.gradle
sed -i s/kotlin-template-core/your-project-name-core/ your-project-name-main/build.gradle
rm -rf .git
git init
git add .
git commit -m "Initial import"
git add remote origin your-project-remote-repo-url
git push -u origin master
```
### Start working on you project
Now you're ready to work on you awesome project; you can build and test you project issuing `./gradlew check`.

If you're pushing your project to [GitLab](https://gitlab.com), you'll see a pipeline execution starting whenever you push a changeset.