package com.darwinsw.goofy

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class HelloTest {
    @Test
    fun shouldSayHelloWorld() {
        assertThat(Hello().sayHello("World"), `is`(equalTo("Hello, World!")))
    }
}