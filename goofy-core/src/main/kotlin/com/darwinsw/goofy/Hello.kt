package com.darwinsw.goofy

class Hello {
    fun sayHello(to: String) = "Hello, ${to}!"
}