package com.darwinsw.goofy

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.util.StdDateFormat
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.StringHttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = arrayOf(ClockApiApplication::class))
open class ClockApiApplication {
}

class Hello(private val message : String) {
    fun sayHelloWorld() = "$message, World!"
}

@Configuration
open class MainModule : WebMvcConfigurer {
    @Bean
    open fun hello() : Hello = Hello("Hi")

    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
    }

    override fun configureMessageConverters(converters: MutableList<HttpMessageConverter<*>?>) {
        converters.add(jacksonConverter())
        converters.add(stringMessageConverter())
    }

    @Bean
    open fun stringMessageConverter(): StringHttpMessageConverter? {
        return StringHttpMessageConverter()
    }

    private fun jacksonConverter(): MappingJackson2HttpMessageConverter? {
        val mapper = ObjectMapper()
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        mapper.dateFormat = StdDateFormat()
        val jacksonConverter = MappingJackson2HttpMessageConverter()
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        jacksonConverter.objectMapper = mapper
        return jacksonConverter
    }
}

fun main(args: Array<String>) {
    runApplication<ClockApiApplication>(*args)
}