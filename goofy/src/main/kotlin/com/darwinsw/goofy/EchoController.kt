package com.darwinsw.goofy

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api/echo")
class EchoController {
    @RequestMapping(method = [RequestMethod.GET])
    fun echo(msg: String): EchoData {
        return EchoData(msg)
    }
}

data class EchoData(val message: String)