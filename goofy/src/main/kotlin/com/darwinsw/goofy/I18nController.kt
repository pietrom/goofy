package com.darwinsw.goofy

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@RestController
@RequestMapping("api/i18n")
class I18nController {
    @RequestMapping("/{lang}", method = [RequestMethod.GET])
    fun lang(@PathVariable("lang") lang: String): TranslationsData {
        return when (lang) {
            "it" -> of("Benvenuti!")
            "en" -> of("Welcome!")
            "es" -> of("¡Bienvenidos!")
            else -> throw UnknownLanguageException(lang)
        }
    }

    fun of(welcome: String) : TranslationsData = TranslationsData(HomeData(welcome, "[${welcome.toUpperCase()}]"))
}

//data class LangData(val translations: TranslationsData)

data class TranslationsData(val home: HomeData)

data class HomeData(val welcome: String, val subtitle: String)

class UnknownLanguageException(val lang: String) : java.lang.Exception("Unknown language $lang")

@ControllerAdvice
class UnknownLanguageExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(value = [(UnknownLanguageException::class)])
    fun handleUserAlreadyExists(ex: UnknownLanguageException,request: WebRequest): ResponseEntity<Any> {
        return ResponseEntity(HttpStatus.NOT_FOUND)
    }
}