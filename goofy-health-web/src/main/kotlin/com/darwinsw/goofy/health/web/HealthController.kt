package com.darwinsw.goofy.health.web

import com.darwinsw.goofy.health.core.HealthService
import com.darwinsw.goofy.health.core.HealthStatus
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("health")
class HealthController(private val service: HealthService) {
    @RequestMapping(method = [RequestMethod.GET])
    fun healthCheck(): HealthStatus {
        return service.check()
    }
}