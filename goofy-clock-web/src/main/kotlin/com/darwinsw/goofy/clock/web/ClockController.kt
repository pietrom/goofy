package com.darwinsw.goofy.clock.web

import com.darwinsw.goofy.clock.core.Clock
import com.darwinsw.goofy.clock.core.ClockData
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api/clock")
class ClockController(private val clock: Clock) {
    @RequestMapping(method = [RequestMethod.GET])
    fun now(): ClockData {
        return clock.now()
    }
}
