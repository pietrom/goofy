package com.darwinsw.goofy.clock.web

import com.darwinsw.goofy.clock.core.Clock
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class Emitter(val key: String, val emitter: SseEmitter, _active: Boolean) {
    var active: Boolean = _active
        private set

    fun deactivate() {
        active = false
    }
}

class Emitters {
    private val emitters = mutableListOf<Emitter>()

    fun emit(id: Long, message: String) {
//        println("Send '$message' to ${emitters.size} available emitters")
        val iterator = emitters.iterator()
        while(iterator.hasNext()) {
            val current = iterator.next()
            if(current.active) {
                try {
                    val builder = SseEmitter
                            .event()
                            .id(String.format("[%d:%s]", id, current.key))
                            .data(message, MediaType.TEXT_PLAIN)
                            .comment("A JSON event, with id $id, for ${current.key}")
                    current.emitter.send(builder)
                } catch (e: Throwable) {
                    println("Error sending to ${current.key}: ${e.message}")
                }
            } else {
                println("Removing emitter ${current.key}")
                iterator.remove()
            }
        }
    }

    fun deactivate(key: String) {
        val choice = emitters.find { e -> e.key == key }
        choice?.deactivate()
        choice?.emitter?.complete()
    }

    fun register(key: String, emitter: SseEmitter) {
        emitters.add(Emitter(key, emitter, true))
    }
}

@Controller
@RequestMapping("events/hearth-beat")
class HearthBeatController(val clock: Clock) {
    companion object {
        private var progr: Long = 0
    }

    init {
        val service = Executors.newSingleThreadScheduledExecutor()

        service.scheduleAtFixedRate(::job, 2, 3, TimeUnit.SECONDS);
    }

    private fun job(): Unit {
        try {
            progr++
            val message = String.format("Goofy HearthBeat #%d at %s", progr, clock.now().toString())
            emitters.emit(progr, message)
        } catch(ex: Exception) {
            println("Error in JOB execution")
            ex.printStackTrace()
        }
    }

    private val emitters = Emitters()

    @RequestMapping("register")
    fun register(key: String): SseEmitter {
        val emitter = SseEmitter()
        emitter.onCompletion {
            println("Client $key disconnected")
            emitters.deactivate(key)
        }
        emitter.onError {
            println("Client $key in error")
            emitters.deactivate(key)
        }
        emitter.onTimeout {
            println("Client $key timed out")
            emitters.deactivate(key)
        }

        emitters.register(key, emitter)
        println("Add emitter $key")
        return emitter
    }
//
//    @RequestMapping("unregister")
//    fun unregister(key: String): UnsubscriptionInfo {
//        val emitter = emitters.remove(key)
//        emitter?.complete()
//        println("Remove emitter $key")
//        println(emitters.size)
//        return UnsubscriptionInfo(key)
//    }
}

data class HearthBeat(val id: Long, val data: Any)

data class UnsubscriptionInfo(val key: String)